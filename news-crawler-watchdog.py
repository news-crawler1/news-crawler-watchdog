#!/usr/bin/env python3
# _*_ coding:utf-8 _*_
import sys
import os
import logging
import logging.handlers
import time
import signal
import datetime
import json
import urllib.request
import urllib.parse
import urllib.error
import configparser
import random
import argparse


# version and dependency checks
if sys.version_info < (3,7,0):
    print("Python 3.7 or higher is required!")



# pylint: disable=invalid-name
class NewsChecker:
    def __init__(self, args):
        # disable traceback on ctrl + c
        signal.signal(signal.SIGINT, lambda x, y: sys.exit(0))

        # get current dir
        self.path = os.path.dirname(os.path.abspath(__file__))

        # arg parse
        self.args = args

        # load options from config
        self.config = self.get_config(self.path)
        self.crawl_interval = self.config.getint('settings', 'crawl_interval', fallback=3600)

        self.ajax_url = self.config.get('ajax', 'ajax_url')
        self.ajax_user = self.config.get('ajax', 'ajax_user')
        self.ajax_pw = self.config.get('ajax', 'ajax_pw')

        self.mail_host = self.config.get('mail', 'mail_host')
        self.mail_port = self.config.get('mail', 'mail_port')
        self.mail_from = self.config.get('mail', 'mail_from')
        self.mail_pw = self.config.get('mail', 'mail_pw')
        self.mail_to = self.config.get('mail', 'mail_to')


        # logger
        if self.args.quiet:
            logging_level = logging.ERROR
        else:
            logging_level = self.config.getint('settings', 'logging_level', fallback=0)

        self.log = self.setup_logger(logging_level)

        self.log.info("News Checker started!")


        # declarations
        self.error = ""
        self.last_log_timestamp = time.time()
        self.known_errors = []

        self.loop()



    def loop(self):
        date = False

        while True:
            today = datetime.date.today()

            if date != today:
                date = today
                # reset known errors
                self.known_errors = []


            self.check()

            if self.error:
                self.log.error(self.error)

            time.sleep(self.crawl_interval * 2)



    def check(self):
        self.error = ""

        self.check_error_log()
        self.check_thresholds()



    def check_error_log(self):
        post = {
            "user"      : self.ajax_user,
            "pw"        : self.ajax_pw,
            "request"   : "crawler_log",
            "level"     : "30",
            "timestamp" : self.last_log_timestamp
        }

        content_raw = self.get_url_content(self.ajax_url, post=post)
        if not content_raw:
            self.log.error("No connection to news-crawler-web!")
            return False

        result = False

        #pylint: disable=bare-except, broad-except
        try:
            json_data = json.loads(content_raw)
        except Exception:
            self.log.exception("Could not load Log JSON from AJAX!\n%s", content_raw)
            return False

        for result in json_data:
            if "msg" not in result:
                return

            self.error += result["msg"] + "\n"

        if result and 'timestamp' in result:
            self.error = "Log Errors:\n__________________________________\n" + self.error + "\n\n"
            self.last_log_timestamp = float(result["timestamp"])



    def check_thresholds(self):
        post = {
            "user"      : self.ajax_user,
            "pw"        : self.ajax_pw,
            "request"   : "crawler_statistics"
        }

        content_raw = self.get_url_content(self.ajax_url, post=post)
        if not content_raw:
            self.log.error("No connection to news-crawler-web!")
            return False

        #pylint: disable=bare-except, broad-except
        try:
            json_data = json.loads(content_raw)
        except Exception:
            self.log.exception("Could not load Statistics JSON from AJAX!\n%s", content_raw)
            return False

        if "items" not in json_data or "links" not in json_data:
            self.log.error("Statistics JSON invalid!")

        thresholds = json_data["links"]

        for data in json_data["items"].values():
            for item in data:
                if item not in thresholds:
                    continue

                threshold_min = thresholds[item]['threshold_min']
                threshold_max = thresholds[item]['threshold_max']
                proc = thresholds[item]['proc']

                if data[item] is None:
                    data[item] = 0

                if threshold_min is not None \
                and threshold_min in data \
                and data[threshold_min] is not None \
                and float(data[item]) < float(data[threshold_min]):
                    key = f"{data['name']}: {item} ({data[item]}) < "\
                          f"{threshold_min} ({data[threshold_min]})"

                    if key not in self.known_errors:
                        self.known_errors.append(key)
                        self.error += key + "\n\n"

                        if proc:
                            self.error += self.get_detail(item, data['id']) + "\n\n"


                if threshold_max is not None \
                and threshold_max in data \
                and data[threshold_max] is not None \
                and float(data[item]) > float(data[threshold_max]):
                    key = f"{data['name']}: {item} ({data[item]}) > "\
                          f"{threshold_max} ({data[threshold_max]})"

                    if key not in self.known_errors:
                        self.known_errors.append(key)
                        self.error += key + "\n\n"

                        if proc:
                            self.error += self.get_detail(item, data['id']) + "\n\n"




    def get_detail(self, item, source):
        post = {
            "user"      : self.ajax_user,
            "pw"        : self.ajax_pw,
            "request"   : "detail",
            "item"      : item,
            "source"    : source
        }

        content_raw = self.get_url_content(self.ajax_url, post=post)

        if not content_raw:
            self.log.error("No Content for Detail!\nItem: %s | Source: %s", item, source)
            return ""

        #pylint: disable=bare-except, broad-except
        try:
            json_data = json.loads(content_raw)
        except Exception:
            self.log.exception("Could not load Detail JSON from AJAX!\n%s", content_raw)
            return ""

        if "name" not in json_data or "data" not in json_data:
            self.log.error("Detail JSON invalid!")

        #print(json_data)

        return json_data["name"] + "\n" + self.results_to_table(json_data["data"])




    def results_to_table(self, results):
        rows = []
        widths = {}

        # structure data and get column widths
        for result in results:
            if not rows:
                cols = []

                count = 0
                for title in result:
                    c = title[:50]
                    cols.append(c)
                    col_len = len(c)

                    if count not in widths:
                        widths[count] = col_len
                    elif widths[count] < col_len:
                        widths[count] = col_len
                    count += 1

                rows.append(cols)

            cols = []

            count = 0
            for col in result:
                c = str(result[col])[:50]
                cols.append(c)
                col_len = len(c)

                if count not in widths:
                    widths[count] = col_len
                elif widths[count] < col_len:
                    widths[count] = col_len
                count += 1

            rows.append(cols)

        # print
        r = ""

        for row in rows:
            count = 0

            for col in row:
                r += col.ljust(widths[count]) + " | "
                count += 1

            r = r[:-3] + "\n"
            if r.count("\n") == 1:
                r += "—" * (sum(widths.values()) + len(widths.values()))
                r += "\n"

        return r + "\n"



    def get_config(self, path):
        config_path = os.path.join(path, 'config.ini')
        config = configparser.ConfigParser(allow_no_value=True, comment_prefixes="#")

        try:
            config.read(config_path)
        except configparser.Error:
            pass

        if not config.has_section('settings'):
            config.add_section('settings')

        if not config.has_option('settings', 'check_interval'):
            config.set('settings', 'check_interval', '3600')

        if not config.has_option('settings', 'logging_level'):
            config.set('settings', 'logging_level', '20')

        if not config.has_section('mail'):
            config.add_section('mail')

        if not config.has_option('mail', 'mail_to'):
            config.set('mail', 'mail_to', "")
        if not config.has_option('mail', 'mail_from'):
            config.set('mail', 'mail_from', "")
        if not config.has_option('mail', 'mail_pw'):
            config.set('mail', 'mail_pw', "")
        if not config.has_option('mail', 'mail_host'):
            config.set('mail', 'mail_host', "")
        if not config.has_option('mail', 'mail_port'):
            config.set('mail', 'mail_port', "587")


        if not config.has_section('ajax'):
            config.add_section('ajax')

        if not config.has_option('ajax', 'ajax_url'):
            config.set('ajax', 'ajax_url', "")
        if not config.has_option('ajax', 'ajax_user'):
            config.set('ajax', 'ajax_user', "")
        if not config.has_option('ajax', 'ajax_pw'):
            config.set('ajax', 'ajax_pw', "")


        with open(config_path, 'w', encoding="utf-8") as configfile:
            config.write(configfile)

        return config



    def setup_logger(self, logging_level):
        log = logging.getLogger(__name__)
        log.setLevel(logging.DEBUG)

        # output logs to stdout
        handler = logging.StreamHandler(sys.stdout)
        handler.setLevel(logging_level)
        log.addHandler(handler)

        handler = logging.handlers.SMTPHandler(
            mailhost = [self.mail_host, self.mail_port],
            fromaddr = self.mail_from,
            toaddrs = self.mail_to,
            subject = "[News Crawler] Error",
            credentials = [self.mail_from, self.mail_pw],
            secure=[],
            timeout=200
        )

        handler.setLevel(logging.ERROR)
        log.addHandler(handler)

        return log



    def get_url_content(self, url, user_agents = None, post = None):
        max_retries = 10
        retries = 0

        request = False

        post_data = None
        if post:
            post_data = urllib.parse.urlencode(post).encode()

        # if user_agents is specified, a random user_agent from the list is picked
        headers = {}
        if user_agents is not None:
            headers['User-Agent'] = random.choice(user_agents)

        url = urllib.request.Request(
            url,
            headers=headers,
            data=post_data
        )

        while True:
            #pylint: disable=broad-except
            try:
                with urllib.request.urlopen(url, timeout=60) as request:
                    content = request.read().decode("utf8", errors='replace')
                    request.close()
                    break

            except urllib.error.HTTPError:
                return False

            # urllib.error.URLError
            except Exception as exc:
                retries += 1
                if retries > max_retries:
                    print('Retry Error', type(exc), exc)
                    return False

                time.sleep(60)

        return content



def setup_arg_parser():
    parser = argparse.ArgumentParser(
        description = 'A checker for the news-crawler'
    )

    parser.add_argument(
        '-q', '--quiet',
        action="store_true",
        help="No output, except errors"
    )

    parser.add_argument(
        '-d', '--daemon',
        action="store_true",
        help="Daemonize (run in background)"
    )

    return parser.parse_args()




if __name__ == "__main__":
    arguments = setup_arg_parser()

    if arguments.daemon:
        import daemon

        arguments.quiet = True

        with daemon.DaemonContext():
            NewsChecker(arguments)

    else:
        NewsChecker(arguments)
