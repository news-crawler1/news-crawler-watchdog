# news-crawler-watchdog
This is a watchdog for [news-crawler](https://gitlab.com/abocado/news-crawler), 
that notifies you with an email once thresholds are exceeded.  

The thresholds can be configured in the dashboard of [news-crawler-web](https://gitlab.com/abocado/news-crawler-web).  
It also sends an email on log entries with level ERROR or CRITICAL.  

This watchdog can also be run from other locations, to ensure notification emails 
are sent even if the server running news-crawler is offline.

## Requirements
python 3.7+  
[news-crawler-web](https://gitlab.com/abocado/news-crawler-web)  


## Getting started
`git clone https://gitlab.com/news-crawler1/news-crawler-watchdog.git`  
All commands require you to be in the projects root directory  
`cd news-crawler-watchdog`  

Copy `config.example.ini` to `config.ini`.  

Insert news-crawler-web credentials and path to news-crawler-web/php/ajax.php 
in `config.ini` [ajax] section  
You also need to configure email credentials in `config.ini` to enable the watchdog to send mail. 

The watchdog can be run like this:  
`python news-crawler-watchdog.py`  

or as background service:  
`nohup python news-crawler-watchdog.py -q &`  
